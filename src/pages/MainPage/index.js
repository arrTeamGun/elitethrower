import React from "react";
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import TextField from '@mui/material/TextField';
import {useState} from 'react';

import { Typography } from "@mui/material";



const MainPage = () => 
{
    const [count, setCount] = useState(0);
    return(
        <Paper sx={{ flexGrow: 1 }} elevation={3} >
        <Grid container spacing={2} columns={16}>
          <Grid item xs={10}>
            <TextField
                id="rand-output"
                label="Number"
                value={count}
                defaultValue="Your number is"
                InputProps={{
                    readOnly: true,
                }}
            />
            <ButtonGroup variant="contained" aria-label="dices button">
                <Button onClick={() => setCount(Math.floor((Math.random() * (4 - 1 + 1)) + 1))}>
                    d4
                </Button>
                <Button onClick={() => setCount(Math.floor((Math.random() * (6 - 1 + 1)) + 1))}>
                  d6
                </Button>
                <Button onClick={() => setCount(Math.floor((Math.random() * (8 - 1 + 1)) + 1))}>
                  d8
                </Button>
                <Button onClick={() => setCount(Math.floor((Math.random() * (10 - 1 + 1)) + 1))}>
                  d10
                </Button>
                <Button onClick={() => setCount(Math.floor((Math.random() * (12 - 1 + 1)) + 1))}>
                  d12
                </Button>
                <Button onClick={() => setCount(Math.floor((Math.random() * (20 - 1 +1)) + 1))}>
                  d20
                </Button>
            </ButtonGroup>
          </Grid>
          <Grid item xs={6}>
            <Typography>Players</Typography>
          </Grid>
        </Grid>
      </Paper>
    )
}

export default MainPage;